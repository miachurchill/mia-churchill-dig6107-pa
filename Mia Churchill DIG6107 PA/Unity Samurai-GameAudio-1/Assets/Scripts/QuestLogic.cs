using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestLogic : MonoBehaviour
{
    [Header("FMOD")]

    public FMODUnity.StudioEventEmitter pickupChime;
    public FMODUnity.StudioEventEmitter questSpeech;
    public FMODUnity.StudioEventEmitter noSword;
    public FMODUnity.StudioEventEmitter questFinishSpeech;
    public FMODUnity.StudioEventEmitter daikoVolume;
    public FMODUnity.StudioEventEmitter toOutro;
    public FMODUnity.StudioEventEmitter attenuation;
    public FMODUnity.StudioEventEmitter soldier2Intro;
    public FMODUnity.StudioEventEmitter soldier2Outro;
    public FMODUnity.StudioEventEmitter soldier3Intro;
    public FMODUnity.StudioEventEmitter solider3Outro;

    [Header("Triggers")]
    public GameObject pickupTrigger;
    public GameObject endquestTrigger;
    public GameObject soldier2Trigger;
    public GameObject soldier3Trigger;

    [Header("UI Elements")]
    public GameObject pressToTalkUI;
    public GameObject pickupUI;
    public GameObject giveUI;
    public GameObject dontHaveSwordUI;
    public GameObject swordUIImage;

    [Header("Items")]
    public GameObject pickupInventory;
    public GameObject emperorsSword;
    public GameObject emperorsSwordEnd;

    bool questComplete;
    public bool questStarted;
    public bool playerHasEnteredSwordTrigger;
    public bool playerHasSword;
    public bool questFinished;

    // Start is called before the first frame update

    void Start()
    {
        playerHasSword = false;
        pickupUI.SetActive(false);
        giveUI.SetActive(false);
        dontHaveSwordUI.SetActive(false);
        pressToTalkUI.SetActive(false);
        pickupInventory.SetActive(false);
        questComplete = false;
        emperorsSwordEnd.SetActive(false);
        swordUIImage.SetActive(false);
        isspeaking = false;
        questFinished = false;
    }

    // Update is called once per frame
    void Update()
    {
        Soldier2Start();
        Soldier3Start();
        SwordQuestStart();
        NoSword();
        PickupSword();
        EndQuest();
        Soldier2Finish();
        Soldier3Finish();
        Timer();
        Attenuation();
    }

     void Soldier2Start(){

        if (soldier2Trigger.GetComponent<Soldier2Trigger>().playerIsInSoldier2Trigger == true && questFinished == false){

            if (Input.GetKeyDown(KeyCode.E) && !isspeaking){

                soldier2Intro.SendMessage("Play");

                isspeaking = true;
            }
        }
    }

     void Soldier3Start(){

        if (soldier3Trigger.GetComponent<Soldier3Trigger>().playerIsInSoldier3Trigger == true && questFinished == false){

            if (Input.GetKeyDown(KeyCode.E) && !isspeaking){

                soldier3Intro.SendMessage("Play");

                isspeaking = true;
            }
        }
    }

    void SwordQuestStart()
    {
        if (endquestTrigger.GetComponent<EndQuestTrigger>().playerInEndTrigger == true && questStarted == false)
        {

            pressToTalkUI.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E) && !isspeaking)
            {

                questSpeech.SendMessage("Play");

                isspeaking = true;

                questStarted = true;
                pressToTalkUI.SetActive(false);

                daikoVolume.SetParameter("Daiko", 1);
            }
        }

    }

    void NoSword(){

        if (endquestTrigger.GetComponent<EndQuestTrigger>().playerInEndTrigger == true && questStarted == true){

            if (playerHasSword == false && Input.GetKeyDown(KeyCode.E) && !isspeaking){

                    dontHaveSwordUI.SetActive(true);
                    noSword.SendMessage("Play");

                    isspeaking = true;
                
            }
        }
    }

    void PickupSword()
    {
        if (questStarted == true && pickupTrigger.GetComponent<SwordPickup>().playerCanPickupSword == true)
        {
            if (playerHasSword == false)
            {
                pickupUI.SetActive(true);
                if (Input.GetKeyDown(KeyCode.E))
                {

                    dontHaveSwordUI.SetActive(false);
                    giveUI.SetActive(true);

                    pickupUI.SetActive(false);
                    Destroy(emperorsSword);
                    playerHasSword = true;
                    swordUIImage.SetActive(true);

                    pickupChime.SendMessage("Play");
                }

            }
          
        }

    }

    void EndQuest(){

        if (playerHasSword == true && endquestTrigger.GetComponent<EndQuestTrigger>().playerInEndTrigger== true)
        {
            giveUI.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E) && !isspeaking)
            {

                questFinishSpeech.SendMessage("Play");

                isspeaking = true;

                playerHasSword = false;
                giveUI.SetActive(false);
                swordUIImage.SetActive(false);

                toOutro.SetParameter("To Outro", 1);

                questFinished = true;
            }
        }
    }

    void Soldier2Finish(){

        if (soldier2Trigger.GetComponent<Soldier2Trigger>().playerIsInSoldier2Trigger == true && questFinished == true){

            if (Input.GetKeyDown(KeyCode.E) && !isspeaking){

                soldier2Outro.SendMessage("Play");

                isspeaking = true;
            }
        }
    }

    void Soldier3Finish(){

        if (soldier3Trigger.GetComponent<Soldier3Trigger>().playerIsInSoldier3Trigger == true && questFinished == true){

            if (Input.GetKeyDown(KeyCode.E) && !isspeaking){

                solider3Outro.SendMessage("Play");

                isspeaking = true;
            }
        }
    }

    float t = 0;

    bool isspeaking;

    void Timer(){

        if (isspeaking){

            t += (1 * Time.deltaTime);

            if(t > 6){

                isspeaking = false;
                t = 0;
            }
        }
    }

      void Attenuation()
    {

        if (isspeaking == true)
        {

            attenuation.SetParameter("Attenuation", 1);

        }

        else{

            attenuation.SetParameter("Attenuation", 0);
        }
    }

}


