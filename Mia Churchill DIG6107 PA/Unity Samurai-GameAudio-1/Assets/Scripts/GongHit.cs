using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GongHit : MonoBehaviour
{

    public bool playerIsInGongTrigger;

    public FMODUnity.StudioEventEmitter gongHit;

    bool allowgong;

    // Start is called before the first frame update
    void Start()
    {
        playerIsInGongTrigger = false;
        allowgong = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerIsInGongTrigger && Input.GetKeyDown(KeyCode.E) && allowgong){
            gongHit.SendMessage("Play");

            allowgong = false;
        }
    }
    void OnTriggerEnter(Collider player)
    {
        playerIsInGongTrigger = true;
    }

    void OnTriggerExit(Collider player)
    {
        
        playerIsInGongTrigger = false;
        allowgong = true;
    }
    
}
