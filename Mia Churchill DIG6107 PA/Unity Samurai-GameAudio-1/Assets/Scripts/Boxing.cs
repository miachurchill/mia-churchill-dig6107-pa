using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boxing : MonoBehaviour
{

    [Header("FMOD")]
    public FMODUnity.StudioEventEmitter throwPunch;

    void PlayBoxingGrunt(){

        throwPunch.SendMessage("Play");
    }
}
