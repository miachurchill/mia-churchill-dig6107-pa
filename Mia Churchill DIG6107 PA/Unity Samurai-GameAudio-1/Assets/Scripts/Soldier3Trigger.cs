using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soldier3Trigger : MonoBehaviour
{
      public bool playerIsInSoldier3Trigger;
    // Start is called before the first frame update
    void Start()
    {
        playerIsInSoldier3Trigger = false;
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {
            playerIsInSoldier3Trigger = true;
        }
    }

    void OnTriggerExit(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {
            playerIsInSoldier3Trigger = false;
        }
    }
}

