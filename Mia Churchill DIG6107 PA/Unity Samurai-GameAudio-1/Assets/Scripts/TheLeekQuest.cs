using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheLeekQuest : MonoBehaviour
{
    [Header("FMOD")]
    
    public FMODUnity.StudioEventEmitter soldier1IntroSpeech;
    public FMODUnity.StudioEventEmitter soldier1OutroSpeech;
    public FMODUnity.StudioEventEmitter questSpeech;

    public FMODUnity.StudioEventEmitter noLeekSpeech;

    public FMODUnity.StudioEventEmitter pickupChime;

    public FMODUnity.StudioEventEmitter leekFinishSpeech;

    public FMODUnity.StudioEventEmitter stringVolume;

    public FMODUnity.StudioEventEmitter toChorus;

    public FMODUnity.StudioEventEmitter attenuation;

    [Header("Triggers")]
    public GameObject leekTrigger;
    public GameObject marketSellerTrigger;
    public GameObject soldier1Trigger;

    [Header("UI Elements")]
    public GameObject pressToTalkUI;
    public GameObject pressToPickUpUI;
    public GameObject pressToGiveUI;
    public GameObject leekUIImage;

    [Header("Items")]
    public GameObject leekToCollect;
    public GameObject leekToGive;
    public bool questStarted;
    public bool questFinished;

    bool doesPlayerHaveLeek;
    // Start is called before the first frame update
    void Start()
    {
        questStarted = false;
        pressToGiveUI.SetActive(false);
        pressToPickUpUI.SetActive(false);
        pressToTalkUI.SetActive(false);
        leekToGive.SetActive(false);
        leekUIImage.SetActive(false);
        isspeaking = false;
        questFinished = false;

    }

    // Update is called once per frame
    void Update()
    {
        LeekPickup();
        MarketSellerStart();
        Soldier1Start();
        NoLeeks();
        MarketSellerEnd();
        Soldier1Finish();
        Timer();
        Attenuation();
    }

    void Soldier1Start(){

        if (soldier1Trigger.GetComponent<Soldier1Trigger>().playerIsInSoldier1Trigger == true && questFinished == false){

            if (Input.GetKeyDown(KeyCode.E) && !isspeaking){

                soldier1IntroSpeech.SendMessage("Play");

                isspeaking = true;
            }
        }
    }

    void MarketSellerStart()
    {
        if (marketSellerTrigger.GetComponent<MarketSellerTrigger>().playerIsInMarketSellerTrigger == true && questStarted == false)
        {
            pressToTalkUI.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E) && !isspeaking)
            {

                questSpeech.SendMessage("Play");

                isspeaking = true;

                questStarted = true;
                pressToTalkUI.SetActive(false);

                stringVolume.SetParameter("Strings", 1);
            }
        }
    }

    void NoLeeks()
    {

        if (marketSellerTrigger.GetComponent<MarketSellerTrigger>().playerIsInMarketSellerTrigger == true && questStarted == true)
        {

            if (doesPlayerHaveLeek == false && Input.GetKeyDown(KeyCode.E) && !isspeaking)
            {

                noLeekSpeech.SendMessage("Play");
                isspeaking = true;
            }
        }
    }
    void LeekPickup()
    {
        if (questStarted == true && leekTrigger.GetComponent<LeekPickup>().playerIsInLeekTrigger == true)
        {
            if (doesPlayerHaveLeek == false)
            {
                pressToPickUpUI.SetActive(true);
                if (Input.GetKeyDown(KeyCode.E))
                {
                    pressToPickUpUI.SetActive(false);
                    Destroy(leekToCollect);
                    doesPlayerHaveLeek = true;
                    leekUIImage.SetActive(true);

                    pickupChime.SendMessage("Play");
                }
            }
        }

        if (questFinished == true && leekTrigger.GetComponent<LeekPickup>().playerIsInLeekTrigger == true){

            pressToPickUpUI.SetActive(false);
            Destroy(leekToCollect);
            leekUIImage.SetActive(false);
        }
    }
    void MarketSellerEnd()
    {
        if (doesPlayerHaveLeek == true && marketSellerTrigger.GetComponent<MarketSellerTrigger>().playerIsInMarketSellerTrigger == true)
        {
            pressToGiveUI.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E) && !isspeaking)
            {

                leekFinishSpeech.SendMessage("Play");

                isspeaking = true;

                doesPlayerHaveLeek = false;
                pressToGiveUI.SetActive(false);
                leekToGive.SetActive(true);
                leekUIImage.SetActive(false);

                toChorus.SetParameter("To Chorus", 1);

                questFinished = true;
            }
        }
    }

    void Soldier1Finish(){

        if (soldier1Trigger.GetComponent<Soldier1Trigger>().playerIsInSoldier1Trigger == true && questFinished == true){

            if (Input.GetKeyDown(KeyCode.E) && !isspeaking){

                soldier1OutroSpeech.SendMessage("Play");

                isspeaking = true;
            }
        }
    }

    float t = 0;

    bool isspeaking;

    void Timer()
    {

        if (isspeaking)
        {

            t += (1 * Time.deltaTime);

            if (t > 6)
            {

                isspeaking = false;
                t = 0;
            }
        }
    }

    void Attenuation()
    {

        if (isspeaking == true)
        {

            attenuation.SetParameter("Attenuation", 1);

        }

        else{

            attenuation.SetParameter("Attenuation", 0);
        }
    }


}
