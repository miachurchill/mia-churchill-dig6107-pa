using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soldier1Trigger : MonoBehaviour
{
    public bool playerIsInSoldier1Trigger;
    // Start is called before the first frame update
    void Start()
    {
        playerIsInSoldier1Trigger = false;
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {
            playerIsInSoldier1Trigger = true;
        }
    }

    void OnTriggerExit(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {
            playerIsInSoldier1Trigger = false;
        }
    }
}
