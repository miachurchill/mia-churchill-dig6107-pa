using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoliageCollision : MonoBehaviour
{
    [Header("Player Info")]

    GameObject playerObject;

    Transform playerPosition;

    Rigidbody playerRigidbody;

    float playerVelocity;

    Vector3 xMove;

    Vector3 zMove;

    [Header("Playback")]
    public float playBackThreshold = 1f;

    float playBackSpeed = 0f;


    [Header("FMOD")]
    [FMODUnity.EventRef]
    public string FoliageSound = "";

    // Start is called before the first frame update
    void Start()
    {
        playerObject = GameObject.Find("ThirdPersonController");
        playerPosition = playerObject.transform;
        playerRigidbody = playerObject.GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
        GetPlayerVelocity();


    }

    void GetPlayerVelocity()
    {

        zMove.z = Mathf.Abs(playerRigidbody.velocity.z);
        xMove.x = Mathf.Abs(playerRigidbody.velocity.x);

        playerVelocity = (zMove.z + xMove.x);

        //Debug.Log("Player velocity = "+playerVelocity);
    }

    void OnTriggerStay(Collider player)
    {

        if (player.gameObject.tag == "Player")
        {

            if (playerVelocity > 0.5f)
            {
                playBackSpeed += (playerVelocity * Time.deltaTime);
                if (playBackSpeed > playBackThreshold)
                {
                    FMODUnity.RuntimeManager.PlayOneShot(FoliageSound, playerPosition.position);

                    playBackSpeed = 0f;
                }

            }
        }


    }

    void test(){
        
    }

}
