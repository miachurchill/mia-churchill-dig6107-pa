using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soldier2Trigger : MonoBehaviour
{
    public bool playerIsInSoldier2Trigger;
    // Start is called before the first frame update
    void Start()
    {
        playerIsInSoldier2Trigger = false;
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {
            playerIsInSoldier2Trigger = true;
        }
    }

    void OnTriggerExit(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {
            playerIsInSoldier2Trigger = false;
        }
    }
}
