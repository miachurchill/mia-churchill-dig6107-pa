using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorKnock : MonoBehaviour
{
    // Start is called before the first frame update
     public bool playerIsInDoorTrigger;

    public FMODUnity.StudioEventEmitter doorKnock;

    bool allowknock;

    // Start is called before the first frame update
    void Start()
    {
        playerIsInDoorTrigger = false;
        allowknock = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerIsInDoorTrigger && Input.GetKeyDown(KeyCode.E) && allowknock){
            doorKnock.SendMessage("Play");

            allowknock = false;
        }
    }
    void OnTriggerEnter(Collider player)
    {
        playerIsInDoorTrigger = true;
    }

    void OnTriggerExit(Collider player)
    {
        
        playerIsInDoorTrigger = false;
        allowknock = true;
    }
}
