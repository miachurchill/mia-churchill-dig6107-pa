using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class Footsteps : MonoBehaviour
{
    //FMOD Stuff
    [Header("FMOD")]
    public FMODUnity.StudioEventEmitter footstepEmitter;

    //Player Velocity Stuff
    GameObject playerObject;
    Rigidbody playerRB;
    Vector3 v;

    [Header("Player Velocity")]
    [SerializeField]
    float playerVelocity;
    public float movementGate;

    public float runWalkTransition;

    public float walkCrouchTransition;

    [Header("Footstep Gate")]
    public float resetModifier;
    public float resetThreshold;

    //Timer
    float t = 0f;
    //Active footstep bool
    bool footstepActive;

    [Header("Switcher")]
    [SerializeField]
    string[] surfaceType = { "Dirt", "Grass", "Stone", "Wood", "Water"};

    [SerializeField]
    string terrainTag;

    // Start is called before the first frame update
    void Start()
    {
        playerObject = GameObject.Find("ThirdPersonController");
        footstepActive = false;
        playerRB = playerObject.GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
        Timer();
        GetPlayerVelocity();
        RayCastSwitch();
        //StateSwitch();
        SpeedSwitch();
        
    }

    void GetPlayerVelocity()
    {
        v.x = Mathf.Abs(playerRB.velocity.x);
        v.z = Mathf.Abs(playerRB.velocity.z);

        playerVelocity = (v.x + v.z);

    }

    void PlayRunFootstep()
    {
        if (!footstepActive)
        {
            if ((playerVelocity > movementGate) && (playerVelocity > runWalkTransition))
            {
                footstepEmitter.SendMessage("Play");
                footstepActive = true;
            }
        }

    }

    void PlayWalkFootstep()
    {
        if (!footstepActive)
        {
            if ((playerVelocity > movementGate) && (playerVelocity < runWalkTransition))
            {

                footstepEmitter.SendMessage("Play");
                footstepActive = true;
            }
        }
    }

    void PlayCrouchFootstep(){
        if (!footstepActive)
        {
            if ((playerVelocity > movementGate) && (playerVelocity < walkCrouchTransition))
            {
                footstepEmitter.SendMessage("Play");
                footstepActive = true;
            }
        }
    }
    /*
    bool isrunning;

    bool iswalking;

    void StateSwitch()
    {

        if (playerVelocity > 4)
        {
            isrunning = true;
            iswalking = false;
        }

        else if (playerVelocity < 4)
        {
            isrunning = false;
            iswalking = true;
        }

        if (isrunning)
        {
            footstepEmitter.SetParameter("Speed", 0);

        }

        else if (iswalking)
        {
            footstepEmitter.SetParameter("Speed", 1);
        }
    }
*/
    void SpeedSwitch(){

        if (Input.GetKeyDown(KeyCode.LeftShift)){
            footstepEmitter.SetParameter("Walk", 1);
            footstepEmitter.SetParameter("Run", 0);
            footstepEmitter.SetParameter("Crouch", 0);
        }

        if (Input.GetKeyDown(KeyCode.C)){
            footstepEmitter.SetParameter("Walk", 0);
            footstepEmitter.SetParameter("Run", 0);
            footstepEmitter.SetParameter("Crouch", 1);
        }

        else {
            footstepEmitter.SetParameter("Walk", 0);
            footstepEmitter.SetParameter("Run", 1);
            footstepEmitter.SetParameter("Crouch", 0);
        }
    }

    void Timer()
    {
        if (footstepActive)
        {
            t += (2 * Time.deltaTime) + resetModifier;
            if (t > resetThreshold)
            {
                t = 0f;
                footstepActive = false;
            }
        }
    }

    void RayCastSwitch()
    {

        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit))
        {
            terrainTag = hit.collider.gameObject.tag;
            Debug.Log("Terrain Tag = " + terrainTag);

        }

        foreach (string i in surfaceType)
        {
            if (terrainTag == i)
            {
                float f = Array.IndexOf(surfaceType, i);
                footstepEmitter.SetParameter("SurfaceType", f);
            }
        }
    }


}
