using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Situps : MonoBehaviour
{
   
    [Header("FMOD")]
    public FMODUnity.StudioEventEmitter situps;

    void PlaySitupGrunt(){

        situps.SendMessage("Play");
    }
}
